# SCP Challenge

## Installation / Environment setup
### pyenv

Install pyenv unless you already have it installed.

```
git clone https://github.com/yyuu/pyenv.git ~/.pyenv
echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.bashrc
echo 'export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.bashrc
echo 'eval "$(pyenv init -)"' >> ~/.bashrc
source ~/.bashrc
```

### python

Install python unless you already have it installed.

```
pyenv install 2.7.6
pyenv rehash
pyenv versions
```

### pyenv virtualenv

Install pyenv-virtualenv unless you already have it installed.

```
git clone https://github.com/pyenv/pyenv-virtualenv.git $(pyenv root)/plugins/pyenv-virtualenv
echo 'eval "$(pyenv virtualenv-init -)"' >> ~/.bashrc
echo 'export VIRTUAL_ENV_DISABLE_PROMPT=1' >> ~/.bashrc
source ~/.bashrc
```

### Project setup - local
#### Cloning git repo

```
git clone git@bitbucket.org:nishant_nasa/itest_scp.git
```

#### Create virtualenv

Move to the root directory of project `bakery-challenge`, if not already there. Create virtual environment for the project.

```
cd itest_scp
pyenv virtualenv 2.7.6 itest_scp-2.7.6
pyenv local itest_scp-2.7.6
pyenv versions
```

#### install requirements / modules

Install python modules.

```
pip install -r requirements.txt
```

## Run / Test
### Run

Run the app from root directory `bakery-challenge`.

```
python main.py
```

### Test
#### DocTest

```
python -m doctest -v main.py
```
