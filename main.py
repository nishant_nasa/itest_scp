import boto3

def divisible_by_3(num):
    """
    For the passed parameter value <@Integer>, it checks for divisibility by 3
    >>> divisible_by_3(3)
    True
    >>> divisible_by_3(7)
    False
    """
    return num%3 == 0


def divisible_by_5(num):
    """
    For the passed parameter value <@Integer>, it checks for divisibility by 5
    >>> divisible_by_5(5)
    True
    >>> divisible_by_5(7)
    False
    """
    return num%5 == 0


def connect_to_S3():
    """
    Assuming aws cli is configured with access and secret access keys
    """
    return boto3.client('s3', 'ap-southeast-2')


def main(max_range):
    """
    For the increments of 1 till max_range <@Integer>:
        if counter is divisible by 3, result is "fizz"
        if counter is divisible by 5, result is "buzz"
        if counter is divisible by both 3 and 5, result is "fizzbuzz"
        else the result is the counter itself
    Output is written to a file, that is uploaded to S3 bucket
        Uploaded file is not be publicly accessible
        Uploaded file can be accessed by few people, assuming their canonical IDs are available in envrionemnt variables
    Delete temporary local output file
    """
    with open("output.txt", "wb") as output:
        for counter in xrange(1, max_range+1):
            if divisible_by_5(counter) and divisible_by_3(counter):
                output.write("fizzbuzz\n")
            elif divisible_by_3(counter):
                output.write("fizz\n")
            elif divisible_by_5(counter):
                output.write("buzz\n")
            else:
                output.write("%d\n" %(counter))

    s3 = connect_to_S3()
    s3.upload_file(
        'output.txt', 'test-scp', 'output.txt',
        ExtraArgs= {
            'GrantFullControl': 'id="ENV[User-Canonical-ID1]"',
            'GrantRead': 'id="ENV[User-Canonical-ID2]"'
        }
    )

if __name__ == '__main__':
    main(100)

